package com.tankgame.EngineHelpers;



import com.tankgame.GameObjects.ID;

import java.awt.*;
import java.awt.image.BufferedImage;

public class BackGround {

    private BufferedImage bg;
    int X, Y;


    public BackGround(int x, int y, ID id) {
        X = x;
        Y = y;
        bg = ResourceLoader.getImage("/resources/bg.png");
    }


    public void render(Graphics g) {
        g.drawImage(bg, (int) X, (int) Y, null);

    }


}
