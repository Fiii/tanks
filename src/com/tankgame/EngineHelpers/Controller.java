package com.tankgame.EngineHelpers;


import com.tankgame.MainController.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller extends Canvas implements ActionListener {
    private static final long serialVersionUID = 1L;
    Game game;
    JButton SButton;
    JFrame gmenu;

    public Controller(Game game) {
        this.game = game;

        gmenu = new JFrame("MENU");

        gmenu.setPreferredSize(new Dimension(200, 300));
        gmenu.setMinimumSize(new Dimension(200, 300));
        gmenu.setMinimumSize(new Dimension(200, 300));
        gmenu.setResizable(false);
        gmenu.setLocationRelativeTo(null);
        gmenu.setVisible(true);

        SButton = new JButton("START");
        SButton.setSize(80, 30);
        SButton.setLocation(60, 60);
        SButton.setVisible(true);
        SButton.addActionListener(this);
        gmenu.add(SButton);
    }

    public void actionPerformed(ActionEvent e) {
        Object x = e.getSource();

        if (x == SButton) {

            gmenu.setVisible(false);
            game.start();

        }

    }

}
