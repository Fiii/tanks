package com.tankgame.EngineHelpers;

import com.tankgame.GameObjects.GameObjects;

import java.awt.*;
import java.util.ArrayList;


public class Engine {

    public ArrayList<GameObjects> gameobjects = new ArrayList<GameObjects>();

    public void tick() {

        for (int i = 0; i < gameobjects.size(); i++) {
            GameObjects GO = gameobjects.get(i);
            GO.tick();
        }
    }

    public void render(Graphics g) {

        for (int i = 0; i < gameobjects.size(); i++) {
            GameObjects GO = gameobjects.get(i);
            GO.render(g);
        }
    }

    public void addObject(GameObjects addObject) {
        gameobjects.add(addObject);
    }


    public void removeObject(GameObjects remObject) {
        gameobjects.remove(remObject);
    }

    public void removeObject(int index) {
        gameobjects.remove(index);
    }


}
