package com.tankgame.EngineHelpers;


import com.tankgame.MainController.Game;

import javax.swing.*;
import java.awt.*;

/**
 * Tworzenie okna startowego. Konstruktor definiowany w spos�b "dynamiczny" - rozmiary okien etc.
 */
public class Window extends Canvas {
    private static final long serialVersionUID = 1L;
    private static Game game;


    public Window(int width, int height, String title, Game game) {

        JFrame window = new JFrame(title);


        this.game = game;

        window.setPreferredSize(new Dimension(width, height));
        window.setMinimumSize(new Dimension(width, height));
        window.setMinimumSize(new Dimension(width, height));


        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.add(game);
        window.setVisible(true);

    }


}
