package com.tankgame.GameObjects.BadGuys;



import com.tankgame.EngineHelpers.Engine;
import com.tankgame.GameObjects.GameObjects;
import com.tankgame.GameObjects.ID;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public abstract class Enemy extends GameObjects {
Engine engine;
EnemyShoot ES;
EnemyHud EH;


	public Enemy(int x, int y, ID id,Engine engine,BufferedImage enemyimage) {
		super(x, y, id, enemyimage);
		this.engine = engine;
		
		//tank shoot ++ hud
		
		ES = new EnemyShoot(x,y,ID.EnemyShoot,engine,this);
		EH = new EnemyHud(x-50,y,ID.EnemyHud,engine,this);
		
		engine.addObject(ES);
		engine.addObject(EH);
		
		
	}
	
	public void tick() {
		X-=moveX;
		//Y++;


		if(Y>=450||Y<=420)
			moveY*=-1;
		
		this.X = GameObjects.stopper((int)X,500, 800);
		collision();
	}
	
	
	public void render(Graphics g) {
		g.drawImage(image, (int)X, (int)Y, null);
	}
	
	private void collision(){
		for(int i=0;	i<engine.gameobjects.size();	i++){
			GameObjects tempObject = engine.gameobjects.get(i);
			
			if(tempObject.getId() == ID.Shot){
				if(this.getBounds().intersects(tempObject.getBounds())){
					engine.removeObject(this);
					engine.removeObject(EH);
					engine.removeObject(ES);
					
					engine.addObject(new EnemyTank(850,435,ID.Enemy,engine));
					engine.removeObject(EH);
					engine.removeObject(ES);

				}
			}
		}
	}
	

	//----------------------setters + getters------------------------
		public void setMoveXSpeed(int x){
		this.moveX = x;
		}
		
		public void setMoveYSpeed(int y){
			this.moveY = y;
			}
		
		
		public void setImage(BufferedImage x){
		this.image = x;
		}
	
		public void setX(int x){
			this.X = x;
		}
		public void setY(int y){
			this.Y = y;
		}

		public double getX(){
			return X;
		}
		
		public double getY(){
			return Y;
		}
		
		public void setID(ID id){
			this.id = id;
		}
		
		public ID getId(){
			return id;
		}
		
		public void setVelX(int velX){
			this.velX = velX;
		}
		public void setVelY(int velY){
			this.velY = velY;
		}
		
		public double getVelX(){
			return velX;
		}
		
		public double getVelY(){
			return velY;
		}
	
	
	
	public Rectangle getBounds() {
		
		return null;
	}

}
