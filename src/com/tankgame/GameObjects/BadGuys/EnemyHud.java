package com.tankgame.GameObjects.BadGuys;

import com.tankgame.EngineHelpers.Engine;
import com.tankgame.GameObjects.GameObjects;
import com.tankgame.GameObjects.ID;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class EnemyHud extends GameObjects {
	public static int HEALTH = 200;
	private int greenValue = 255;
	private int score = 0;
	private int level = 1;
	Enemy enemy;
	
	public EnemyHud(int x, int y, ID id,Engine engine,Enemy enemy) {
		super(x, y, id,null);
		this.enemy = enemy;
		// TODO Auto-generated constructor stub
	}

	public void tick(){
		
		this.X-=enemy.moveX;
		this.X = GameObjects.stopper((int) X, 500, 800);
		this.Y = enemy.Y;
	
		HEALTH = GameObjects.stopper(HEALTH, 0, 30);
		greenValue = GameObjects.stopper(greenValue, 0, 255);
		
		greenValue = HEALTH * 2;
		//score++;
	}
	
	public void render(Graphics g){
		g.setColor(Color.gray);
		g.fillRect((int)X,(int) Y, 20, 3);
		g.setColor(new Color(75,greenValue,0));
		g.fillRect((int)X,(int) Y, HEALTH, 4);
		g.setColor(Color.white);
		g.drawRect((int)X,(int)Y, 30, 4);
		
	}
	
	public void score(int score){
		this.score = score;	
	}
	
	public int getScore(){
		return score;
	}
	
	public int getLevel(){
		return level;
	}
	
	public void setLevel(int level){
		this.level=level;
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}
}
