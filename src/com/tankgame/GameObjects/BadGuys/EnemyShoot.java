package com.tankgame.GameObjects.BadGuys;

import com.tankgame.EngineHelpers.Animation;
import com.tankgame.EngineHelpers.Engine;
import com.tankgame.EngineHelpers.Mouse;
import com.tankgame.EngineHelpers.ResourceLoader;
import com.tankgame.GameObjects.GameObjects;
import com.tankgame.GameObjects.ID;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


public class EnemyShoot extends GameObjects {
	
	Engine engine;
	double  v0,posx,posy,time,vx,vy,dt;
	double angle,rad,dab;
	boolean shot = false;
	boolean ashot = false;
	boolean shotposition;
	GameObjects object;
	Animation explosion;
	public Thread thread2;
	
	public EnemyShoot(int x, int y, ID id,Engine engine,GameObjects object) {
		super(x, y, id,null);
		this.engine = engine;
		this.object =  object;
		v0 =120;
		angle = 45;
		dt = 0.1; // s
		vx = v0 * Math.cos(Math.PI / 180 * angle);
		vy = v0 * Math.sin(Math.PI / 180 * angle); 
		X = object.X; // m
		Y =  object.Y;  // m
		time = 0.09; // s
		explosion = ResourceLoader.getAnimation("/resources/explosion.png", 64, 64, 5, 50);
	}

	@Override
	public void tick() {
	
		X+=object.getMX();	
		Y+=object.getMY();
		
		if(true){
		   X -=  vx * dt;
		   Y -= vy * dt;
		   time += dt;
		   vy -= 9.82 * dt;
		}		
		   if(X>=5000||X<=-5000||Y>=5000){
			   setPara(); 
		   }
	}
	
	public void setPara(){
		
		rad = Math.sqrt((Mouse.X)*(Mouse.X)+(Mouse.Y)*(Mouse.Y));
		dab = Math.toDegrees(Math.sin(Mouse.Y/rad));
		
		v0 = 120;//Powah
		angle =	45;//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		vx = v0 * Math.cos(Math.PI / 180 * angle);
		vy = v0 * Math.sin(Math.PI / 180 * angle); 
		dt = 0.09; // s
		
		vy -= 9.82 * dt;
		X =  object.X; // md
		Y =  object.Y; // m
		time =1; // s
		shot = false;
		explosion.playedOnce=false;
		explosion.setFrame(1);
		
	}
	
	
	public void render(Graphics g) {

		g.setColor(Color.GRAY);
		g.fillOval((int)X,(int)Y+4,6, 6);
	}
	
	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return new Rectangle((int)X,(int)Y,8,8);
	}

}
