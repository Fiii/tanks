package com.tankgame.GameObjects;


import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class GameObjects {
    public double X, Y, velX, velY;
    public ID id;
    public boolean shot;
    public int moveX, moveY;
    public BufferedImage image;

    public GameObjects(int x, int y, ID id, BufferedImage image) {
        this.X = x;
        this.Y = y;
        this.id = id;
        this.image = image;
    }

    public abstract void tick();

    public abstract void render(Graphics g);

    public abstract Rectangle getBounds();

    //blocking objects
    public static int stopper(int var, int min, int max) {
        if (var >= max) {
            var = max;
        } else if (var <= min) {
            var = min;
        }
        return var;
    }

    //----------------------setters + getters------------------------


    public void setX(int x) {
        this.X = x;
    }

    public void setY(int y) {
        this.Y = y;
    }

    public int getMX() {
        return moveX;
    }

    public int getMY() {
        return moveY;
    }

    public double getX() {
        return X;
    }

    public double getY() {
        return Y;
    }

    public void setID(ID id) {
        this.id = id;
    }

    public ID getId() {
        return id;
    }

    public void setVelX(int velX) {
        this.velX = velX;
    }

    public void setVelY(int velY) {
        this.velY = velY;
    }

    public double getVelX() {
        return velX;
    }

    public double getVelY() {
        return velY;
    }

}
