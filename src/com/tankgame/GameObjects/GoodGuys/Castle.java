package com.tankgame.GameObjects.GoodGuys;

import com.tankgame.EngineHelpers.Engine;
import com.tankgame.EngineHelpers.ResourceLoader;
import com.tankgame.GameObjects.GameObjects;
import com.tankgame.GameObjects.HUD;
import com.tankgame.GameObjects.ID;

import java.awt.*;

public class Castle extends GameObjects {
    //BufferedImage castle;
    Engine engine;


    public Castle(int x, int y, ID id, Engine engine) {
        super(x, y, id, ResourceLoader.getImage("/resources/castle.png"));
        new ResourceLoader();
        //castle = ResourceLoader.getImage("castle.png");
        this.engine = engine;
        engine.addObject(new HUD(890, 250, ID.Base, engine));
        //	engine.addObject(new CastleShot(30,490,ID.CastleShot,engine));
    }

    public void tick() {


    }

    @Override
    public void render(Graphics g) {
        g.drawImage(image, (int) X, (int) Y, null);
    }


    @Override
    public Rectangle getBounds() {
        // TODO Auto-generated method stub
        return new Rectangle(850, 250, 200, 200);
    }

}
