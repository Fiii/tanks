package com.tankgame.GameObjects;

import com.tankgame.EngineHelpers.Engine;

import java.awt.*;

public class HUD extends GameObjects {
    public static int HEALTH = 200;
    private int greenValue = 255;
    private int score = 0;
    private int level = 1;


    public HUD(int x, int y, ID id, Engine engine) {
        super(x, y, id, null);
        // TODO Auto-generated constructor stub
    }

    public void tick() {
        HEALTH = GameObjects.stopper(HEALTH, 0, 100);
        greenValue = GameObjects.stopper(greenValue, 0, 255);

        greenValue = HEALTH * 2;
        //score++;
    }

    public void render(Graphics g) {
        g.setColor(Color.gray);
        g.fillRect((int) X, (int) Y, 100, 7);
        g.setColor(new Color(75, greenValue, 0));
        g.fillRect((int) X, (int) Y, HEALTH, 8);
        g.setColor(Color.white);
        g.drawRect((int) X, (int) Y, 100, 8);

        //g.drawString("Score: "+score,15,64);
        //	g.drawString("Level: "+level,15,80);
    }

    public void score(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public Rectangle getBounds() {
        // TODO Auto-generated method stub
        return null;
    }
}
