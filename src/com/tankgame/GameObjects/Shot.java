package com.tankgame.GameObjects;


import com.tankgame.EngineHelpers.Animation;
import com.tankgame.EngineHelpers.Engine;
import com.tankgame.EngineHelpers.Mouse;
import com.tankgame.EngineHelpers.ResourceLoader;

import java.awt.*;

public class Shot extends GameObjects {

    Engine engine;
    double v0, posx, posy, time, vx, vy, dt, power;
    double angle, rad, dab;
    boolean shot = false;

    boolean shotposition;
    boolean fire = false;
    GameObjects object;
    Animation explosion;
    ShotHUDPower SHP;


    public Shot(int x, int y, ID id, Engine engine, GameObjects object) {
        super(x, y, id, null);
        this.engine = engine;
        this.object = object;

        SHP = new ShotHUDPower(10, 50, ID.SPHUD, engine);
        engine.addObject(SHP);
        explosion = ResourceLoader.getAnimation("/resources/explosion.png", 64, 64, 5, 50);
    }

    @Override
    public void tick() {


        if (fire) {
            power += 3;
            SHP.HEALTH++;
            power = GameObjects.stopper((int) power, 0, 200);
        }

        if (!shot) {
            X += object.velX;
            this.X = GameObjects.stopper((int) X, 40, 470);
        }

        if (X >= 870 && Y >= 290)
            this.X = GameObjects.stopper((int) X, 40, 870);


        if (shot) {

            X += vx * dt;
            Y -= vy * dt;
            time += dt;
            vy -= 9.82 * dt;

            explosion.update();
            engine.addObject(new Trail((int) X + 18, (int) Y + 4, ID.Trail, Color.YELLOW, 8, 8, 0.1f, engine));
            collision();
        }
        if (X >= 1000 || X <= 40 || Y >= 550) {
            setPara();
            power = 0;
        }
        explosion.setDelay(50);
        if (shotposition) {
            explosion.setPosition(object.X + 10, object.Y - 35);
            shotposition = false;
        }
    }


    public void setPara() {

        rad = Math.sqrt((Mouse.X) * (Mouse.X) + (Mouse.Y) * (Mouse.Y));
        dab = Math.toDegrees(Math.sin(Mouse.Y / rad));

        v0 = power;//Powah
        angle = 35 - dab;//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        vx = v0 * Math.cos(Math.PI / 180 * angle);
        vy = v0 * Math.sin(Math.PI / 180 * angle);
        dt = 0.2; // s

        vy -= 9.82 * dt;
        X = object.X; // md
        Y = object.Y; // m
        time = 1; // s
        shot = false;
        explosion.playedOnce = false;
        explosion.setFrame(1);
    }


    public void render(Graphics g) {

        g.setColor(Color.blue);
        g.drawString("Use mouse UP and DOWN to set Angle", 10, 10);
        g.drawString(("Angle: " + (45 - dab)), 10, 25);
        g.drawString("Power:" + power, 10, 40);
        if (shot == true) {
            if (!explosion.hasPlayedOnce())

                explosion.paint(g);
            g.setColor(Color.yellow);
            g.fillOval((int) X + 18, (int) Y + 4, 6, 6);
        }
        if (shotposition) {
            explosion.setPosition(object.X + 10, object.Y - 35);
            shotposition = false;
        }

    }


    private void collision() {

        for (int i = 0; i < engine.gameobjects.size(); i++) {
            GameObjects tempObject = engine.gameobjects.get(i);

            if (tempObject.getId() == ID.Castle) {


                if (getBounds().intersects(tempObject.getBounds())) {
                    //collision code
                    HUD.HEALTH--;

                }
            }
        }
    }


    @Override
    public Rectangle getBounds() {
        // TODO Auto-generated method stub
        return new Rectangle((int) X, (int) Y, 8, 8);
    }

}
